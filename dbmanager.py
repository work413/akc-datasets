import mysql.connector
import csv

# The class that creates and fills the database.
class WriteDataToDB:
    # Represent the instancte of the self object.
    def __init__(self):
        self._dbname = "akc" # The name of the db.
        self._breeds_file = "datasets/breeds.csv" # The path to the breeds file, change this if the file isn't in the dataset folder.
        self._intell_file = "datasets/intelligence.csv" # The path to the intelligence file, change this if the file isn't in the dataset folder.
        self._popul_file = "datasets/popularity.csv" # The path to the popularity file, change this if the file isn't in the dataset folder.
  
    # Sets the database name.
    def set_dbname(self, name):
        self._dbname = name
    
    # Sets the file path to the breeds file.
    def set_breeds_file(self, path):
        self._breeds_file = path
  
    # Sets the file path to the intelligence file.
    def set_intell_file(self, path):
        self._intell_file = path

    # Sets the file path to the popularity file.
    def set_popul_file(self, path):
        self._popul_file = path
    
    # Creates a new database.
    def createDB(self):
        # Connects to server through the root user.
        db = mysql.connector.connect(user='root', host='127.0.0.1')
        cursor = db.cursor()
        cursor.execute("CREATE DATABASE " + self._dbname) # Creates database.
        cursor.execute("USE " + self._dbname)

        self.create_tables(cursor) # Create tables after creating database.
        self.insert_breed_data(cursor) # Insert Breed data after creating tables.
        self.insert_intell_data(cursor) # Insert Breed Intelligence data after creating tables.
        self.insert_popul_data(cursor) # Insert Breed Popularity after creating tables.
        self.create_view(cursor) # Creates the 'ranking_avg' view.
        db.commit() # Saves changes to database.
        return db
    
    # Creates 'breeds', 'breed_intell' and 'breed_popul' tables for the database.
    def create_tables(self, cursor):
        #If there are no tables, create them.
        cursor.execute("""CREATE TABLE breeds
                (breed VARCHAR(100) NOT NULL, min_height decimal(5, 2), max_height decimal(5, 2), min_weight int, max_weight int, PRIMARY KEY (breed))""")
        cursor.execute("""CREATE TABLE breed_intell
                (breed VARCHAR(100) NOT NULL, classification VARCHAR(100), obedience int, reps_lower int, reps_upper int, PRIMARY KEY (breed))""")
        cursor.execute("""CREATE TABLE breed_popul
                (breed VARCHAR(100) NOT NULL, rank_2016 int, rank_2015 int, rank_2014 int, rank_2013 int, PRIMARY KEY (breed))""")
    

    # Inserts values from csv file into breeds table in database.
    def insert_breed_data(self, cursor):
        with open(self._breeds_file) as file:
          reader = csv.reader(file) # Reads file.
          next(reader) # Skips first line in file.
        
          # Query for inserting breed data.
          query = """INSERT INTO breeds (breed, min_height, max_height, min_weight, max_weight)
                  VALUES (%s, %s, %s, %s, %s)"""
        
          for row in reader: # Iterates through each row in file.
            cursor.execute(query, (row[0], float(row[1] or 0), float(row[2] or 0), int(row[3] or 0), int(row[4] or 0)))
    
    # Inserts values from csv file into breed_intell table in database.
    def insert_intell_data(self, cursor):
        with open(self._intell_file) as file:
            reader = csv.reader(file) # Reads file.
            next(reader) # Skips first line in file.
    
            # Query for inserting breed intelligence data.
            query = """INSERT INTO breed_intell (breed, classification, obedience, reps_lower, reps_upper) 
                   VALUES (%s, %s, %s, %s, %s)"""
        
            for row in reader: # Iterates through each row in file.
              cursor.execute(query, (row[0], row[1], int(row[2] or 0), int(row[3] or 0), int(row[4] or 0)))
    
    # Inserts values from csv files into breed_popul table in database.
    def insert_popul_data(self, cursor):
        with open(self._popul_file) as file:
            reader = csv.reader(file) # Reads file.
            next(reader) # Skips first line in file.
        
            # Query for inserting breed popularity data.
            query = """INSERT INTO breed_popul (breed, rank_2016, rank_2015, rank_2014, rank_2013)
                VALUES (%s, %s, %s, %s, %s)"""
        
            for row in reader: # Iterates through each row in file.
              cursor.execute(query, (row[0], int(row[1] or 0), int(row[2] or 0), int(row[3] or 0), int(row[4] or 0)))
    
    # Creates a view in the database, and grants access for user 'ShowJudge'.
    def create_view(self, cursor):
        cursor.execute("CREATE VIEW ranking_avg AS SELECT AVG(rank_2016) AS 2016_avg, AVG(rank_2015) AS 2015_avg, AVG(rank_2014) AS 2014_avg, AVG(rank_2013) AS 2013_avg FROM breed_popul")
        cursor.execute("GRANT SELECT, INSERT ON ranking_avg TO 'ShowJudge'@'localhost'")
