## American Kennel Club Datasets application

This application is console based and allows the user to view information about different dog breeds from AKC datasets. The database is generated if it doesn't already exists. It uses SQL queries to retrieve data from the database.
