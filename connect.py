import mysql.connector
from mysqlx import ProgrammingError
from dbmanager import WriteDataToDB
from menu import Menu

# Author: Martina Andersson

# The entry point of the application.

# Class for connecting to mysql and database.
class Connect:
  # Represent the instancte of the self object.
  def __init__(self):
      self._connection = mysql.connector.connect(user='root', host='127.0.0.1')
      self._cursor = self._connection.cursor(buffered=True)
      self._db = "akc"
  
  # Print the login menu and asks the user for input.
  def login_menu(self):
      print('\n****** Welcome ******')
      print('What would you like to do? (login/register) :')
      choice = input()
      
      # Calls the login method if user put in 'login'.
      if choice.lower() == 'login':
          self.login_user()
      # Calls the register method is user put in 'register'.
      elif choice.lower() == 'register':
          self.register_user()
      else:
          print('Invalid input')  
          # Reloads the login menu if user put in invalid value.
          self.login_menu() 
      
      self.initialize_menu() # After successfull login, call the method that initializes the main menu.
  
  # Asks the user to register an account and creates that account. 
  def register_user(self):
      print('\nEnter a username:')
      username = input()
      print('\nEnter a password:')
      password = input()

      # If the databse wasn't found, call the method that creates it.
      if self.isdb() == None:
          self.create_db()
      
      # Creates user with SELECT and UPDATE privileges.
      self._cursor.execute("CREATE USER %s@'localhost' IDENTIFIED BY %s", [username, password])
      self._cursor.execute("GRANT SELECT, UPDATE ON * . * TO %s@'localhost'", [username])
      self._cursor.execute("FLUSH PRIVILEGES")
      print("\nUser created.")
  
      # Calls the method that prompts the user to log in.
      self.login_user()
  
  # Returns None if the database wasn't found.
  def isdb(self):
      self._cursor.execute("SHOW DATABASES")

      found = None
      for x in self._cursor:
        if x[0] == self._db:
          found = 1
      
      return found
  
  # Prompts the user to log in.
  def login_user(self):
      print('\nEnter the username:')
      username = input()
      print('\nEnter the password:')
      password = input()

      # Calls the method that tries to connect to the database. 
      self.connect_db(username, password)
  
  # Tries to connect to the database, creates the database if it isn't found.
  def connect_db(self, username, password):
      # Checks if conenction to database is possible.
      try:
          self._connection = mysql.connector.connect(user=username, host='127.0.0.1', password=password, database=self._db)
          self._cursor = self._connection.cursor(buffered=True)
      # If the user doesn't have access rights to the database, prompt her/him to log in again.
      except ProgrammingError:
          print('Wrong username or password.')
          self.login_menu()
      # If for some other reason, connection failed, call the class that creates the database.
      except:
          self.create_db()
          # After creating the database, connect to it through this method.
          self.connect_db(username, password)
  
  # Instansiates the class that creates the database.
  def create_db(self):
      createdb = WriteDataToDB() # Class that creates and fills the database.
      createdb.set_dbname(self._db) # Sets the name of the database.
      createdb.createDB() # Creates the database and calls function that inserts the data(csv-files) into it.

      print('\nDatabase created')
  
  # Instansiates the class with the main menu.
  def initialize_menu(self):
      menu = Menu() # Class that has the main menu.
      menu.set_cursor(self._cursor) # Sets the cursor for the Menu class.
      menu.print_menu() # Prints the main menu.

# The stating point.
connection = Connect()
connection.login_menu()