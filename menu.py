from importlib.util import set_loader
from tabulate import tabulate

# Class for the menu and handling the output to the terminal.
class Menu:
    # Represent the instancte of the self object.
    def __init__(self):
      self._cursor = ""
      self._show_judge = "ShowJudge" # The user who is granted access to the 'all_rankings' view.
      self._judge_user = None # Is set to '1' if the current user is 'ShowJudge'.

    # Sets the cursor.
    def set_cursor(self, cursor):
      self._cursor = cursor
    
    # Prints the main menu to the terminal.
    def print_menu(self):
        print('\n***** Enter one of the following numbers to make a choice *****')
        print('\n1. List all dog breeds.')
        print('2. Search for a specific dog breed.')
        print('3. Filter dog breeds based on intelligence.')
        print('4. Filter dog breeds based on size.')
        print('5. List dog breeds based on popularity.')
        print('6. Filter breeds based on requirements.')
        print('7. Compare breed intelligence with popularity.')

        self._cursor.execute("SELECT CURRENT_USER()") # Find who the current user is.

        for x in self._cursor:
            # If the current user is 'ShowJudge', print this extra menu option.
            if x[0] == self._show_judge + '@localhost':
                self._judge_user = 1
                print('8. Show all popularity rankings(2013-2016).')

        print('x. Enter x to exit application.')
      
        menu_choice = input() # Users choice from the main menu.
        self.handle_choice(menu_choice) # Calls the method that handles the user's choice.

    # Handles the user's choice.
    def handle_choice(self, choice):
        # If the current user is 'ShowJudge' and they would like to see the 'all_rankings' view, call the method for the view.
        if self._judge_user and choice == '8':
            self.judge_view()
        elif choice == '1':
            self.list_breeds()
        elif choice == '2':
            self.search_breed()
        elif choice == '3':
            self.search_intelligence()
        elif choice == '4':
            self.search_size()
        elif choice == '5':
            self.list_from_popularity()
        elif choice == '6':
            self.search_requirements()
        elif choice == '7':
            self.search_compare()
        elif choice == 'x':
            self._cursor.close() # Closes the cursor if user chose to exit application.
            exit()
        else:
            # If any other choice is made, go back to main menu.
            self.print_menu()
    
    # Lists all breeds and counts how many there are.
    def list_breeds(self):
        self._cursor.execute("SELECT breed from breeds")

        for x in self._cursor:
            print(x[0]) # Prints all the names of all breeds in the table.
        
        self._cursor.execute("SELECT COUNT(breed) FROM breeds") # Counts how many there are.

        for x in self._cursor:
            print("\nTotal number of breeds: " + str(x[0])) # Prints the total number of breeds.

        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Prompts the user to search for a breed.
    def search_breed(self):
        print("Enter the name of the breed you would like to search for.")
        breed = input()

        self.find_breed(breed) # Calls the method that searches for the breed in the 'breeds' table.
    
    # Searches for a breed in the 'breeds' table.
    def find_breed(self, breed):
        self._cursor.execute("SELECT * FROM breeds WHERE breed = %s", [breed]) # Searches for the breeds with a matching breed value of input.

        data_list = [] # Will be tabulated.

        for x in self._cursor:
            list_entry = [x[0], str(x[1]), str(x[2]), str(x[3]), str(x[4])] # Populate this list with data about the breed.
            data_list.append(list_entry) # Add this list to the 'data_list' list.
        
        # Prints the tabulated list of data.
        print(tabulate(data_list, ['Breed Name', 'Min height(inches)', 'Max height(inches)', 'Min weight(lbs)', 'Max weight(lbs)'], tablefmt="pretty"))
        
        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Prompts the user to choose intelligence level.
    def search_intelligence(self):
        print("\nThe following are the different levels of intelligence of the dog breeds:")
        
        # Finds all classifications of intelligence.
        self._cursor.execute("SELECT classification, COUNT(*) AS count_entries FROM breed_intell GROUP BY classification ORDER BY count_entries", )

        data_list = [] # Will be tabulated.
        row_count = 1 # Counts rows.

        # Iterates through all classifications of intelligence.
        for x in self._cursor:
            list_entry = [row_count, x[0], str(x[1])]
            data_list.append(list_entry)
            row_count += 1
        
        print(tabulate(data_list, ['Num', 'Intelligence Level', 'Number of Dogs'], tablefmt="pretty")) # Prints the tabulated list of data.
        
        print("\nWhich level of intelligence would you like to choose? (1-" + str(len(data_list)) + ")")

        intelligence_level = int(input()) # User's choice of intelligence.

        # Calls the method that lists breeds from intelligence.
        self.list_breeds_from_intelligence(data_list[intelligence_level - 1][1])
    
    # Lists breeds based of classification of intelligence.
    def list_breeds_from_intelligence(self, level):
        # Finds all breeds with intelligence classification that matches the user's input.
        self._cursor.execute("SELECT * FROM breed_intell WHERE classification = %s", [level])

        data_list = [] # Will be tabulated.

        # Iterates through all breeds that were found based of intelligence.
        for x in self._cursor:
            list_entry = [x[0], x[1], str(x[2]), str(x[3]), str(x[4])] # Populate the list with data about the breed.
            data_list.append(list_entry) # Add the list to the 'data_list' list.
        
        # Prints the tabulated list of data.
        print(tabulate(data_list, ['Breed Name', 'Intelligence level', 'Obedience probability(%)', 'Lower limit of repitition for new commands', 'Upper limit of repititions for new commands'], tablefmt="pretty"))
        
        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Prompts the user to choose the max height.
    def search_size(self):
        print("\nEnter the max height of the breeds you would like to view:")

        max_height = float(input()) # User's input of max height.
        self.list_breeds_from_size(max_height) # Call the method that lists all breeds that fulfills the requirement of max height.
    
    # Lists breeds that fulfills the user's requirement of max height.
    def list_breeds_from_size(self, height):
        # Finds breeds that have a lower max height value than the input.
        self._cursor.execute("SELECT * FROM breeds WHERE breeds.max_height <= %s", [height])

        data_list = [] # Will be tabulated.

        # Iterates through the breeds.
        for x in self._cursor:
            list_entry = [x[0], str(x[1]), str(x[2]), str(x[3]), str(x[4])] # Populate the list with data about the breed.
            data_list.append(list_entry) # Add the list to the 'data_list' list.
        
        # Prints the tabulated list of data.
        print(tabulate(data_list, ['Breed Name', 'Min height(inches)', 'Max height(inches)', 'Min weight(lbs)', 'Max weight(lbs)'], tablefmt="pretty"))
        
        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Lists breeds from popularity average.
    def list_from_popularity(self):
        # Join the 'breeds' and the 'breed_popul' tables to find the average height, weight and intelligence of the breed.
        self._cursor.execute("""SELECT breeds.breed, (breeds.min_height + breeds.max_height) / 2, (breeds.min_weight + breeds.max_weight) / 2,
                            (breed_popul.rank_2016 + breed_popul.rank_2015 + breed_popul.rank_2014 + breed_popul.rank_2013) / 4
                            FROM breeds INNER JOIN breed_popul ON breeds.breed = breed_popul.breed""")
        
        data_list = [] # Will be tabulated.

        # Iterates through the breeds.
        for x in self._cursor:
            list_entry = [x[0], float(x[1]), float(x[2]), float(x[3])] # Populate the list with data about the breed.
            data_list.append(list_entry) # Add the list to the 'data_list' list.
        
        data_list.sort(key=lambda x: x[3]) # Sorts the list based on popularity average.

        # Prints the tabulated list of data.
        print(tabulate(data_list, ['Breed Name', 'Height Average(inches)', 'Weight Average(lbs)', 'Popularity Rank(2013-2016 average)'], tablefmt="pretty"))
        
        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Prompts the user to input requirements for breeds.
    def search_requirements(self):
        print("\nEnter the max height of your preferred dog breed(inches):")
        max_height = input() # The user's choice of max height.

        print("\nEnter the max weight of you preferred dog breed(lbs):")
        max_weight = input() # The user's choice of max weight.

        print("\nWhat is the desired level of intelligence for your preferred dog breed?(1-6)")
        
        # Finds all classifications of intelligence.
        self._cursor.execute("SELECT classification FROM breed_intell GROUP BY classification")
        order = 1
        classifications = []

        # Iterates through all classifications of intelligence.
        for x in self._cursor:
            print(str(order) + ". " + x[0]) # The classification along with the order number.
            classifications.append(x[0]) # Adds the classification to the 'classifications' list.
            order += 1 # Increase the order for the next classification.
        
        intelligence_choice = input() # User's number choice of intelligence level.
        intelligence = classifications[int(intelligence_choice) - 1] # Translate the user's number choice to a classification choice.

        self.filter_requirements(int(max_height), int(max_weight), intelligence) # Call the method that filter the breeds based of user's requirements.
    
    # Filters breeds based of user's requirements.
    def filter_requirements(self, height, weight, intelligence):
        # Finds the breeds that fulfill the requirements.
        self._cursor.execute("""SELECT breeds.breed, breeds.max_height, breeds.max_weight, breed_intell.classification, breed_intell.obedience
                            FROM breeds INNER JOIN breed_intell ON breed_intell.breed = breeds.breed
                            WHERE (breeds.max_height <= %s AND breeds.max_weight <= %s AND breed_intell.classification = %s)""", [height, weight, intelligence]) 
        
        data_list = [] # Will be tabulated.

        # Iterates through the found breeds.
        for x in self._cursor:
            list_entry = [x[0], x[1], x[2], x[3], str(x[4]) + '%'] # Populate the list with data about the breed.
            data_list.append(list_entry) # Adds this list to the 'data_list' list. 
        
        # Prints the tabulated list of data.
        print(tabulate(data_list, ['Breed Name', 'Max Height(inches)', 'Max Weight(lbs)', 'Intelligence Classification', 'Obedience Probability'], tablefmt="pretty"))
        
        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Prompts the user to choose minimum obedience level and popularity average.
    def search_compare(self):
        print("\nEnter the minimum obedience probability(%): ")
        obedience = input() # User's obedience choice.

        print("\nEnter the minimum popularity rank average(1-183): ")
        popularity_avg = input() # User's popularity average choice.

        self.compare_intelligence_popularity(obedience, popularity_avg) # Call the method that compares the obedience with popularity.

    # Finds breeds that fulfill the user's requirement for obedience and popularity.
    def compare_intelligence_popularity(self, obedience, popularity):
        # Joins 'breed_intell' and 'breed_popul' tables on breed name when the breed matches the requirements for both obedience and popularity.
        self._cursor.execute("""SELECT breed_intell.breed, breed_intell.obedience,
                                (breed_popul.rank_2016 + breed_popul.rank_2015 + breed_popul.rank_2014 + breed_popul.rank_2013) / 4
                                FROM breed_intell
                                INNER JOIN breed_popul ON breed_popul.breed = breed_intell.breed
                                AND (breed_popul.rank_2016 + breed_popul.rank_2015 + breed_popul.rank_2014 + breed_popul.rank_2013) / 4 <= %s
                                WHERE obedience >= %s 
                                ORDER BY breed_intell.obedience DESC""", [int(popularity), int(obedience)])
        
     
        data_list = [] # Will be tabulated.

        # Iterates through the found breeds.
        for x in self._cursor:
            list_entry = [x[0], str(x[1]) + '%', str(x[2])] # Populates the list with data about the breed.
            data_list.append(list_entry) # Adds the list to the 'data_list' list.
        
        # Prints the tabulated list of data.
        print(tabulate(data_list, ['Breed Name', 'Obedience Probability', 'Avarage Popularity Rank(2013-2016)'], tablefmt="pretty"))
        
        self.back() # Call the method that handles the option of going back to the main menu.
    
    # Shows the 'all_rankings' view to the ShowJudge user.
    def judge_view(self):
        # Finds the view.
        self._cursor.execute("SELECT * FROM ranking_avg")

        data_list = [] # Will be populated.

        # Iterates through all rows in the view.
        for x in self._cursor:
            list_entry = [x[0], x[1], x[2], x[3]] # Populate the list with data.
            data_list.append(list_entry) # Adds the list to the 'data_list' list.
        
        # Prints the tabulated list of data.
        print(tabulate(data_list, ['2016 Ranking Average', '2015 Ranking Average', '2014 Ranking Average', '2013 Ranking Average'], tablefmt="pretty"))

        self.back() # Call the method that handles the option of going back to the main menu.
 
    # Handles the go back option and calls the method that prints the main method if the user enters 'b'.
    def back(self):
        print("\nEnter b to go back to the main menu")
        key = input() # Goes back to main menu if user enters 'b'.
        if key == 'b':
          self.print_menu()

